# CalendarDemo README

Copyright 2017 F. Almesjo

This file is part of CalendarDemo.

CalendarDemo is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

CalendarDemo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with CalendarDemo.  If not, see <http://www.gnu.org/licenses/>.


This project is a webapp that is a demonstration of a physical product- a calendar- that runs in WebGL (in a browser). The project is made with Blend4Web (GPL v3 version). Because of that, the project is released under the GPL v3, too.

There are five things you can do while running the webapp:

Scrollwheel:			Zoom in/out
Right mouse button:		Move
Left mouse button:		Rotate
Html button "Forward":	Turn one page forward
Html button "Backward":	Turn one page backward