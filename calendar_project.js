/*
Copyright 2017 F. Almesjo

This file is part of CalendarDemo.

CalendarDemo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

CalendarDemo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with CalendarDemo.  If not, see <http://www.gnu.org/licenses/>.
*/

"use strict"

// register the application module
b4w.register("calendar_project_main", function (exports, require) {
    // Fs addition:
    var self = this;

    // import modules used by the app
    var m_app = require("app");
    var m_cfg = require("config");
    var m_data = require("data");
    var m_preloader = require("preloader");
    var m_ver = require("version");

    // Added
    var m_scenes = require("scenes");
    var m_tex = require("textures");
    var m_anim = require("animation");
    var m_cont = require("container");
    //var m_mouse = require("mouse");
    var m_nla = require("nla");

    // detect application mode
    var DEBUG = (m_ver.type() == "DEBUG");

    // automatically detect assets path
    var APP_ASSETS_PATH = m_cfg.get_assets_path("calendar_project");

    // Our "state":
    var state = 0;

    // Images
    var imagefront = new Image();
    imagefront.src = "./assets/1.jpg";
    imagefront.onload = function () {
    };
    var imageback = new Image();
    imageback.src = "./assets/28.jpg";
    imageback.onload = function () {
    };
    var imageabout = new Image();
    imageabout.src = "./assets/26.jpg";
    imageabout.onload = function () {
    };
    var imageglobe = new Image();
    imageglobe.src = "./assets/27.jpg";
    imageglobe.onload = function () {
    };
    var imagejanpic = new Image();
    imagejanpic.src = "./assets/2.jpg";
    imagejanpic.onload = function () {
    };
    var imagejan = new Image();
    imagejan.src = "./assets/3.jpg";
    imagejan.onload = function () {
    };
    var imagefebpic = new Image();
    imagefebpic.src = "./assets/4.jpg";
    imagefebpic.onload = function () {
    };
    var imagefeb = new Image();
    imagefeb.src = "./assets/5.jpg";
    imagefeb.onload = function () {
    };
    var imagemarspic = new Image();
    imagemarspic.src = "./assets/6.jpg";
    imagemarspic.onload = function () {
    };
    var imagemars = new Image();
    imagemars.src = "./assets/7.jpg";
    imagemars.onload = function () {
    };
    var imageaprilpic = new Image();
    imageaprilpic.src = "./assets/8.jpg";
    imageaprilpic.onload = function () {
    };
    var imageapril = new Image();
    imageapril.src = "./assets/9.jpg";
    imageapril.onload = function () {
    };
    var imagemaypic = new Image();
    imagemaypic.src = "./assets/10.jpg";
    imagemaypic.onload = function () {
    };
    var imagemay = new Image();
    imagemay.src = "./assets/11.jpg";
    imagemay.onload = function () {
    };
    var imagejunepic = new Image();
    imagejunepic.src = "./assets/12.jpg";
    imagejunepic.onload = function () {
    };
    var imagejune = new Image();
    imagejune.src = "./assets/13.jpg";
    imagejune.onload = function () {
    };
    var imagejulypic = new Image();
    imagejulypic.src = "./assets/14.jpg";
    imagejulypic.onload = function () {
    };
    var imagejuly = new Image();
    imagejuly.src = "./assets/15.jpg";
    imagejuly.onload = function () {
    };
    var imageaugpic = new Image();
    imageaugpic.src = "./assets/16.jpg";
    imageaugpic.onload = function () {
    };
    var imageaug = new Image();
    imageaug.src = "./assets/17.jpg";
    imageaug.onload = function () {
    };
    var imageseptpic = new Image();
    imageseptpic.src = "./assets/18.jpg";
    imageseptpic.onload = function () {
    };
    var imagesept = new Image();
    imagesept.src = "./assets/19.jpg";
    imagesept.onload = function () {
    };
    var imageoctpic = new Image();
    imageoctpic.src = "./assets/20.jpg";
    imageoctpic.onload = function () {
    };
    var imageoct = new Image();
    imageoct.src = "./assets/21.jpg";
    imageoct.onload = function () {
    };
    var imagenovpic = new Image();
    imagenovpic.src = "./assets/22.jpg";
    imagenovpic.onload = function () {
    };
    var imagenov = new Image();
    imagenov.src = "./assets/23.jpg";
    imagenov.onload = function () {
    };
    var imagedecpic = new Image();
    imagedecpic.src = "./assets/24.jpg";
    imagedecpic.onload = function () {
    };
    var imagedec = new Image();
    imagedec.src = "./assets/25.jpg";
    imagedec.onload = function () {
    };

    var frontfront;
    var frontback;
    var pagefront;
    var pageback;
    var backfront;
    var backback;

    /**
     * export the method to initialize the app (called at the bottom of this file)
     */
    exports.init = function () {
        m_app.init({
            canvas_container_id: "main_canvas_container",
            callback: init_cb,
            show_fps: DEBUG,
            console_verbose: DEBUG,
            autoresize: true
            // Added to test:
            //alpha: true
        });
    }

    /**
     * callback executed when the app is initialized 
     */
    function init_cb(canvas_elem, success) {

        if (!success) {
            console.log("b4w init failure");
            return;
        }

        // Removed for custom splashscreen:
        //m_preloader.create_preloader();

        // ignore right-click on the canvas element
        canvas_elem.oncontextmenu = function (e) {
            e.preventDefault();
            e.stopPropagation();
            return false;
        };

        load();
    }

    /**
     * load the scene data
     */
    function load() {
        //m_data.load(APP_ASSETS_PATH + "calendar-31.json", load_cb, preloader_cb);

        // New code for splashscreen
        var preloader_cont = document.getElementById("preloader_cont");
        preloader_cont.style.visibility = "visible";
        m_data.load(APP_ASSETS_PATH + "calendar-36.json", load_cb, preloader_cb);
    }

    /**
     * update the app's preloader
     */
    //function preloader_cb(percentage) {
    //    m_preloader.update_preloader(percentage);
    //}
    // New code for splashscreen:
    function preloader_cb(percentage) {
        var prelod_dynamic_path = document.getElementById("prelod_dynamic_path");
        var percantage_num = prelod_dynamic_path.nextElementSibling;

        prelod_dynamic_path.style.width = percentage + "%";
        percantage_num.innerHTML = percentage + "%";
        if (percentage == 100) {
            var preloader_cont = document.getElementById("preloader_cont");
            preloader_cont.style.visibility = "hidden";
            return;
        }
    }

    /**
     * callback executed when the scene data is loaded
     */
    function load_cb(data_id, success) {

        if (!success) {
            console.log("b4w load failure");
            return;
        }

        m_app.enable_camera_controls();

        // place your code here
        var canvas_elem = m_cont.get_canvas();

        document.getElementById("fwd-button").addEventListener("click", function (e) {
            if (e.preventDefault)
                e.preventDefault();

            go_fwd();
        }, false);

        document.getElementById("bwd-button").addEventListener("click", function (e) {
            if (e.preventDefault)
                e.preventDefault();

            go_bwd();
        }, false);

        // Initializing objects..
        frontfront = m_scenes.get_object_by_name("Plane");
        frontback = m_scenes.get_object_by_name("Plane.001");
        pagefront = m_scenes.get_object_by_name("Plane.005");
        pageback = m_scenes.get_object_by_name("Plane.004");
        backfront = m_scenes.get_object_by_name("Plane.003");
        backback = m_scenes.get_object_by_name("Plane.002");
        
        m_tex.replace_image(frontfront, "FrontFront", imagefront);
        m_tex.replace_image(backback, "BackBack", imageback);
        //setState(states[0]);

        m_nla.stop();
    }
    
    var states = [14];

    states[0] = {
        frontback: imagejanpic,
        pagefront: imagejan,
        pageback: imagefebpic,
        backfront: imagefeb
    }
    states[1] = {
        frontback: imagejanpic,
        pagefront: imagejan,
        pageback: imagefebpic,
        backfront: imagefeb
    }
    states[2] = {
        frontback: imagefebpic,
        pagefront: imagefeb,
        pageback: imagemarspic,
        backfront: imagemars
    }
    states[3] = {
        frontback: imagemarspic,
        pagefront: imagemars,
        pageback: imageaprilpic,
        backfront: imageapril
    }
    states[4] = {
        frontback: imageaprilpic,
        pagefront: imageapril,
        pageback: imagemaypic,
        backfront: imagemay
    }
    states[5] = {
        frontback: imagemaypic,
        pagefront: imagemay,
        pageback: imagejunepic,
        backfront: imagejune
    }
    states[6] = {
        frontback: imagejunepic,
        pagefront: imagejune,
        pageback: imagejulypic,
        backfront: imagejuly
    }
    states[7] = {
        frontback: imagejulypic,
        pagefront: imagejuly,
        pageback: imageaugpic,
        backfront: imageaug
    }
    states[8] = {
        frontback: imageaugpic,
        pagefront: imageaug,
        pageback: imageseptpic,
        backfront: imagesept
    }
    states[9] = {
        frontback: imageseptpic,
        pagefront: imagesept,
        pageback: imageoctpic,
        backfront: imageoct
    }
    states[10] = {
        frontback: imageoctpic,
        pagefront: imageoct,
        pageback: imagenovpic,
        backfront: imagenov
    }
    states[11] = {
        frontback: imagenovpic,
        pagefront: imagenov,
        pageback: imagedecpic,
        backfront: imagedec
    }
    states[12] = {
        frontback: imagedecpic,
        pagefront: imagedec,
        pageback: imageabout,
        backfront: imageglobe
    }
    states[13] = {
        frontback: imagedecpic,
        pagefront: imagedec,
        pageback: imageabout,
        backfront: imageglobe
    }

    function setState(state) {
        m_tex.replace_image(frontback, "FrontBack", state.frontback);
        m_tex.replace_image(pagefront, "PageFront", state.pagefront);
        m_tex.replace_image(pageback, "PageBack", state.pageback);
        m_tex.replace_image(backfront, "BackFront", state.backfront);
    }
    
    function go_fwd() {

        if (state < 1) {

            state++;
            setState(states[1]);
            open_front_cover();
            return;
        }
        if (state >= 13) {
            if (state > 13) return;

            state++;
            close_back_cover();
            return;
        }
        state++;
        change_tex_before_fwd();
        turn_left();
    }

    function change_tex_before_fwd() {

        switch (state) {
            case 1:
                setState(states[0]);
                break;
            case 2:
                setState(states[1]);
                break;
            case 3:
                setState(states[2]);
                break;
            case 4:
                setState(states[3]);
                break;
            case 5:
                setState(states[4]);
                break;
            case 6:
                setState(states[5]);
                break;
            case 7:
                setState(states[6]);
                break;
            case 8:
                setState(states[7]);
                break;
            case 9:
                setState(states[8]);
                break;
            case 10:
                setState(states[9]);
                break;
            case 11:
                setState(states[10]);
                break;
            case 12:
                setState(states[11]);
                break;
            case 13:
                setState(states[12]);
                break;
        }
    };
    function go_bwd() {
        if (state > 13) {
            state--;
            setState(states[12]);
            open_back_cover();
            return;
        }
        if (state <= 1) {
            if (state < 1) return;
            state--;
            close_front_cover();
            return;
        }
        state--;
        change_tex_before_bwd();
        turn_right();
    }
    function change_tex_before_bwd() {

        switch (state) {
            case 1:
                setState(states[1]);
                break;
            case 2:
                setState(states[2]);
                break;
            case 3:
                setState(states[3]);
                break;
            case 4:
                setState(states[4]);
                break;
            case 5:
                setState(states[5]);
                break;
            case 6:
                setState(states[6]);
                break;
            case 7:
                setState(states[7]);
                break;
            case 8:
                setState(states[8]);
                break;
            case 9:
                setState(states[9]);
                break;
            case 10:
                setState(states[10]);
                break;
            case 11:
                setState(states[11]);
                break;
            case 12:
                setState(states[12]);
                break;
            case 13:
                setState(states[12]);
                break;
        }
    };

    // Animations:
    function open_front_cover() {
        if (m_nla.is_play())
            m_nla.stop();
        m_nla.set_range(1, 21);
        m_nla.set_frame(1);
        m_nla.play();
    }

    function close_front_cover() {
        if (m_nla.is_play())
            m_nla.stop();
        m_nla.set_range(106, 126);
        m_nla.set_frame(106);
        m_nla.play();
    }

    function turn_left() {
        if (m_nla.is_play())
            m_nla.stop();
        m_nla.set_range(22, 42);
        m_nla.set_frame(22);
        m_nla.play();
    }

    function turn_right() {
        if (m_nla.is_play())
            m_nla.stop();
        m_nla.set_range(85, 105);
        m_nla.set_frame(85);
        m_nla.play();
    }

    function open_back_cover() {
        if (m_nla.is_play())
            m_nla.stop();
        m_nla.set_range(64, 84);
        m_nla.set_frame(64);
        m_nla.play();
    }

    function close_back_cover() {
        if (m_nla.is_play())
            m_nla.stop();
        m_nla.set_range(43, 63);
        m_nla.set_frame(43);
        m_nla.play();
    }
});

// import the app module and start the app by calling the init method
b4w.require("calendar_project_main").init();
